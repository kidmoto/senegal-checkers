import sqlite3


class Connect:
    """
    Подключение к базе данных
    (используется только, как родительский класс)
    """

    def __init__(self):
        self.connect = sqlite3.connect('users.db')
        self.cursor = self.connect.cursor()

    def __del__(self):
        self.connect.close()


class Users(Connect):
    """
    Работа с таблицей пользователей
    """

    def create_table(self):
        """
        Создание таблицы пользователей
        """
        
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS users(
            user_id INT PRIMARY KEY,
            user_name TEXT,
            user_password TEXT);    
        ''')
    
    def whrite_users(self, user_id, user_name, user_password):
        """
        Запись данных в таблицу пользователей
        """

        insert_data = (user_id, user_name, user_password)
    
        self.cursor.execute("INSERT INTO users VALUES (?, ?, ?);", insert_data)
        self.connect.commit()

    def read_last_users_id(self):
        """
        Получение id последнего добавленного пользователя
        """

        self.cursor.execute("SELECT * FROM users ORDER BY user_id DESC LIMIT 1;")
        return self.cursor.fetchone()

    def read_users(self):
        """
        Получение всех ранее добавленных в таблицу пользователей
        """

        self.cursor.execute("SELECT * FROM users")
        return self.cursor.fetchall()

