import sys
import hashlib

from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QVBoxLayout
from PyQt5.QtWidgets import QLineEdit, QPushButton
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt

import users
from game import Game


class AuthorizationWindow(QWidget):
    """
    Окно авторизации/регистрации
    """
    
    def __init__(self):
        super().__init__()

        self.init_ui()

        self.users = users.Users()
        self.users.create_table()

        self.flag = True

    def init_ui(self):
        """
        Инициализация интерфейса лаунчера
        """

        # Настройка окна авторизации
        self.setWindowTitle('Авторизация')
        self.setGeometry(300, 300, 400, 200)

        # Настройка шрифта
        self.font = QFont()
        self.font.setPointSize(16)

        # Информационное поле
        self.label = QLabel()
        self.label.setFont(self.font)
        self.label.setAlignment(Qt.AlignCenter)
        self.label.setText('Войдите или зарегистрируйтесь!')

        # Поле ввода имени пользователя
        self.name_field = QLineEdit()
        self.name_field.setPlaceholderText('Логин')

        # Поле ввода пароля
        self.pass_field = QLineEdit()
        self.pass_field.setPlaceholderText('Пароль')

        # Кнопка начала игры с компьютером
        self.with_pc = QPushButton('Играть с компьютером')
        self.with_pc.clicked.connect(self.start_with_pc)

	# Кнопка начала игры с другом
        self.with_user = QPushButton('Играть с другом')
        self.with_user.clicked.connect(self.start_with_user)

        # Кнопка регистрации
        self.registration_btn = QPushButton('Зарегистрироваться')
        self.registration_btn.clicked.connect(self.registration)

        # Кнопка выхода
        self.exit_btn = QPushButton('Выход')
        self.exit_btn.clicked.connect(self.close)

        # Компоновка интерфейса
        v_box = QVBoxLayout()
        v_box.addWidget(self.label)
        v_box.addWidget(self.name_field)
        v_box.addWidget(self.pass_field)
        v_box.addWidget(self.with_pc)
        v_box.addWidget(self.with_user)
        v_box.addWidget(self.registration_btn)
        v_box.addWidget(self.exit_btn)

        self.setLayout(v_box)
 
    def open_game(self, bot):
        """
        Запуск игры
        """

        self.hide()
        QApplication.exit(0)
        self.close()
        game = Game()
        game.bot_activate = bot
        game.start_game()

    def _authorization(self):
        """
        Авторизация пользователя
        """

        name = self.name_field.text()
        password = self.hashing(self.pass_field.text())

        if self.check_user_name(name):
            self.label.setText('Пользователь не найден.')
        elif not self.check_password(name, password):
            self.label.setText('Неверный пароль!')
        else:
            return True

    def start_with_pc(self):
        if self._authorization():
            self.open_game(True)

    def start_with_user(self):
        if self._authorization():
            self.open_game(False)

    def registration(self):
        """
        Регистрация пользователя
        """
        
        user_id = 1
        name = self.name_field.text()
        password = self.hashing(self.pass_field.text())
        last_user = self.users.read_last_users_id()

        if last_user:
            user_id += last_user[0] + 1            

        if self.check_user_name(name):
            self.users.whrite_users(user_id, name, password)
            self.label.setText('Пользователь успешно зарегистрирован!')
        else:
            self.label.setText('Имя пользователя уже существует!')

    def hashing(self, text):
        """
        Хеширование переданного текста
        """

        hash_text = hashlib.md5(text.encode())
        hex_text = hash_text.hexdigest()

        return hex_text

    def check_user_name(self, user_name):
        """
        Проверка имени пользователя (логина)
        """

        flag = True

        if users := self.users.read_users():
            for saved_users in users:
                if user_name == saved_users[1]:
                    flag = False

        return flag
        
    def check_password(self, name, password):
        """
        Проверка имени пароля
        """

        flag = True

        for saved_users in self.users.read_users():
            if name == saved_users[1] and password != saved_users[2]:
                flag = False
        return flag


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = AuthorizationWindow()
    window.show()
    app.exec_()
